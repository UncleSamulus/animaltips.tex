# Animaltips

A damn small latex package for O'Reilly-Like animal tips

<div>
  <img src="./misc/example.png" alt="Animaltips Example (Caption)" width="400"/>
</div>

## Installation

Supposing you have a latex project in `~/myproject`, you can add `animaltips` in your project folder:

```bash
$ git clone https://framagit.org/UncleSamulus/animaltips.tex.git
$ cd animaltips.tex
$ cp -r animaltips ~/myproject/animaltips
```

Then in your main latex file, add the following line:

```latex
\usepackage{animaltips/sty/remarks}

[...]

\begin{document}

\animalLegend % Add the legend

\begin{animalTip}
You can use the package like this
\end{animalTip}
[...]
```


## Usage

### Available command

- `\begin{animalTip} <message> \end{animalTip}`: A tip with a baboon;
- `\begin{animalInfo} <message> \end{animalInfo}`: A note with a owl;
- `\begin{animalWarning} <message> \end{animalWarning}`: A warning with a snake;
- `\begin{animalNote}{animal} <message> \end{animalNote}`: A note with a custom animal (you can set any animal name you want as long as there is a file `animaltips/media/logo/animals/<animal>.png`);
- `\animalLegend`: Add the legend to your document.
